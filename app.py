import numpy as np

import os
import sqlalchemy
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.orm import Session
from sqlalchemy import create_engine, func

from flask import Flask, jsonify


#################################################
# Database Setup
#################################################
engine = create_engine('sqlite:///' + os.path.join('resources', 'hawaii.sqlite'))

# reflect an existing database into a new model
Base = automap_base()
# reflect the tables
Base.prepare(engine, reflect=True)

# Save reference to the table
# id, date, prcp, station, tobs
Measurement = Base.classes.measurement

# elevation, id, latitude, longitude, name, station
State = Base.classes.station

#################################################
# Flask Setup
#################################################
app = Flask(__name__)


#################################################
# Flask Routes
#################################################

@app.route("/")
def welcome():
    """List all available api routes."""
    return (
        f"Available Routes:<br/>"
        f"/api/v1.0/precipitation<br/>"
        f"/api/v1.0/stations<br />"
        f"/api/v1.0/tobs<br />"
        f"/api/v1.0/&lt;start&gt;<br />"
        f"/api/v1.0/&lt;start&gt;/&lt;end&gt;<br />"
    )


@app.route("/api/v1.0/precipitation")
def precipitation():
    session = Session(bind=engine)
    result = session.query(Measurement.id, Measurement.date, Measurement.prcp).all()
    session.close()

    data = []
    for id, date, prcp in result:
        entry = {}
        entry['id'] = id
        entry['date'] = date
        entry['prcp'] = prcp

        data.append(entry)

    return jsonify(data)

@app.route("/api/v1.0/stations")
def stations():
    session = Session(bind=engine)
    result = session.query(Measurement.id, Measurement.date, Measurement.station).all()
    session.close()

    data = []
    for id, date, station in result:
        entry = {}
        entry['id'] = id
        entry['date'] = date
        entry['station'] = station

        data.append(entry)

    return jsonify(data)


@app.route("/api/v1.0/tobs")
def tobs():
    session = Session(bind=engine)
    result = session.query(Measurement.id, Measurement.date, Measurement.tobs).all()
    data = []
    session.close()

    for id, date, tobs in result:
        entry = {}
        entry['id'] = id
        entry['date'] = date
        entry['tobs'] = tobs

        data.append(entry)

    return jsonify(data)



@app.route("/api/v1.0/<start>")
def data_start(start):
    session = Session(bind=engine)
    result = session.query(Measurement.id, Measurement.date, Measurement.prcp, Measurement.station, Measurement.tobs).filter(Measurement.date > start).all()
    session.close()

    data = []
    for id, date, prcp, station, tobs in result:
        entry = {}
        entry['id'] = id
        entry['date'] = date
        entry['prcp'] = prcp
        entry['station'] = station
        entry['prcp'] = tobs

        data.append(entry)

    return jsonify(data)


@app.route("/api/v1.0/<start>/<end>")
def data_start_end(start, end):
    session = Session(bind=engine)
    result = session.query(Measurement.id, Measurement.date, Measurement.prcp, Measurement.station, Measurement.tobs).filter(Measurement.date > start).filter(Measurement.date < end).all()
    session.close()

    data = []
    for id, date, prcp, station, tobs in result:
        entry = {}
        entry['id'] = id
        entry['date'] = date
        entry['prcp'] = prcp
        entry['station'] = station
        entry['prcp'] = tobs

        data.append(entry)

    return jsonify(data)

if __name__ == '__main__':
    app.run(debug=True)
